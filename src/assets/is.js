/**
 * Check if an object property is defined
 *
 * This function will check if a property exists on an object,
 * no matter how deep the property is. You won't have to check
 * each level manually this way.
 *
 * The object and the property you want to check can be supplied in 3 ways.
 * is( obj, 'level1', 'level2', 'level3' );
 * is( obj, 'level1.level2.level3' );
 * both of these check obj.level1.level2.level3
 * is( 'obj.level1.level2.level3' );
 * which checks window.obj.level1.level2.level3
 *
 * MODES
 * There is an internal mode toggle that is set by a wrapper function
 *
 * "isset" is the default mode and functions similar to isset() in php
 * It only checks to see if the property exists, regardless of its value
 * isDefined() is a wrapper that uses is() in "isset" mode
 *
 * "empty" also checks if a property isn't false/empty, similar to empty() in php,
 * the only difference being that is() will not return true if a property value
 * is false|null|''|{}|[]
 *
 * isEmpty() is a wrapper of is() that returns true if a property does not
 * exist, or is otherwise empty and uses is() in "empty" mode.
 *
 * @since 2.0.2
 *
 * @param  object|string target  An object or string representation of
 *                                 of the object path shown in a string.
 * @param  string    props,....  One or more strings denoting the property
 *                                 path. Either submit the path as separate
 *                                 strings or use a single string with
 *                                 periods in between properties.
 * @return Boolean                 if the property exists, returns true
 */

/* function is( target, props ) {

 var emptyMode = ( 'empty' === is.prototype.mode );

 if( ! target ){
 if ( emptyMode ) {
 // empty always should return true as this point
 return false;
 } else if ( target === false && ! props ) {
 return true;
 } else {
 return false;
 }
 }

 var depth = 0;
 var pSet;

 if ( typeof props !== "string" && typeof props !== "number" ) {
 props = target;
 target = window;
 }

 if ( typeof props === "string" && typeof arguments[2] === "undefined" ) {
 pSet = props.split( '.' );
 } else {
 depth += 1;
 pSet = arguments;
 }

 while ( depth < pSet.length ) {

 if ( typeof target != 'object' && typeof target != 'function' || target === null ) {
 return false;
 }
 var branch = pSet[depth];

 if ( !(branch in target) ) {
 return false;
 }

 target = target[branch];
 depth += 1;
 }
 if ( emptyMode ) {

 return ( target ) ? true : false;
 }
 return true;
 }

 is.prototype.mode = 'isset';

 // like isset() in php, see above is() for documentation
 function isDefined( target, props ) {
 is.prototype.mode = 'isset';
 return is.apply( this, arguments );
 }

 // like empty() in php, see above is() for documentation
 function isEmpty( target, props ) {
 is.prototype.mode = 'empty';
 return !is.apply( this, arguments );
 } */
/**
 * query variable
 *
 * Inspect the existence of a property, no matter how deeply nested, or check a variable directly
 *
 * queryVar returns an object with a key called found if the property was found and a key called val
 * containing the value of the property. Doesn't matter how deeply nested the property is,
 * will never return an error and you don't need to check each nested level separately.
 *
 * There are 4 ways of passing parameters to queryVar()
 *
 * 1) pass a single variable
 *  queryVar( nameOfVariable );
 * 2) pass a variable and string based index parameters to check for a property
 * queryVar( nameOfObj, 'indexToCheck', 'propertyToCheck' );
 * 3) pass a string resembling the path of the property to check, using dot notation:
 *  queryVar( 'nameOfObj.indexToCheck.propertyToCheck' );
 * 4) pass the variable and then a single string representation of the path to the property,
 * separating nest level indexes with dots.
 *  queryVar( nameOfObj, 'indexToCheck.propertyToCheck  );
 *
 * Used by isEmpty and isDefined to quickly determine if a property is defined and perform further
 *
 * @param  target	mixed
 * @param  props [...] string
 * @returns { found: (bool), val: (mixed)}
 */
function queryVar(target, props) {
  let depth = 0;
  let pSet;
  let pointer;
  let singular = false;

  if (typeof props !== 'string' && typeof props !== 'number') {
    props = target;
    pointer = window;
    singular = true;
  } else {
    pointer = target;
  }

  if (
    typeof props === 'string' &&
		typeof arguments[2] === 'undefined' &&
		isNaN(props)
  ) {
    pSet = props.split('.');
  } else {
    depth += 1;
    pSet = arguments;
  }

  if (singular && pSet.length === 1) {
    return { found: true, val: target };
  }

  while (depth < pSet.length) {
    if (
      (typeof pointer !== 'object' && typeof pointer !== 'function') ||
			pointer === null
    ) {
      return { found: false };
    }

    const branch = pSet[depth];

    if (!(branch in pointer)) {
      return { found: false };
    }

    pointer = pointer[branch];
    depth += 1;
  }

  return { found: true, val: pointer };
}

/**
 * isDefined
 *
 * Check if a variable or nested property of a variable is set
 *
 * There are 4 ways of passing parameters to isDefined()
 *
 * 1) pass a single variable
 *  isDefined( nameOfVariable );
 * 2) pass a variable and string based index parameters to check for a property
 * isDefined( nameOfObj, 'indexToCheck', 'propertyToCheck' );
 * 3) pass a string resembling the path of the property to check, using dot notation:
 *  isDefined( 'nameOfObj.indexToCheck.propertyToCheck' );
 * 4) pass the variable and then a single string representation of the path to the property,
 * separating nest level indexes with dots.
 *  isDefined( nameOfObj, 'indexToCheck.propertyToCheck  );
 *
 * @param target
 * @param props
 * @returns {boolean}
 */
function isDefined(target, props) {
  const result = queryVar.apply(this, arguments);
  return (
    result.found && !(result.val === null || typeof result.val === 'undefined')
  );
}

/**
 * isEmpty
 *
 * Performs a similar function as empty() in php. Check if a variable or property of a variable
 * is considered empty or not.
 *
 * Empty means one of the following:
 * - the property is not set at all
 * - the property is false
 * - the property is an empty string (without spaces)
 * - the property is an array containing no values, i.e. []
 * - the property is an object with no properties of its own, i.e. {}
 * - the property is a number equal to 0
 *
 *
 *  * There are 4 ways of passing parameters to isEmpty()
 *
 * 1) pass a single variable
 *
 *  isEmpty( nameOfVariable );
 *
 * 2) pass a variable and string based index parameters to check for a property
 *
 * isEmpty( nameOfObj, 'indexToCheck', 'propertyToCheck' );
 *
 * 3) pass a string resembling the path of the property to check, using dot notation:
 *
 *  isEmpty( 'nameOfObj.indexToCheck.propertyToCheck' );
 *
 * 4) pass the variable and then a single string representation of the path to the property,
 * separating nest level indexes with dots.
 *
 *  isEmpty( nameOfObj, 'indexToCheck.propertyToCheck  );
 *
 * @param target
 * @param props
 * @returns {boolean}
 */
function isEmpty(target, props) {
  const result = queryVar.apply(this, arguments);

  if (!result.found) {
    return true;
  }

  const val = result.val;

  if (!val) {
    return true;
  } else if (typeof val === 'function') {
    // always return false if its a function
    return false;
  } else if (typeof val === 'object') {
    // let's check if it is an array or a vanilla object
    if (val.toString() === '[object Object]') {
      // check if object has any own properties, if not, we consider it empty
      for (const prop in val) {
        if (result.val.hasOwnProperty(prop)) {
          return false;
        }
      }

      return true;
    } else if (Object.prototype.toString.call(val) === '[object Array]') {
      // check if array has any values, consider it empty if not
      return !val.length;
    }

    return false;
  } else if (val.length === 0) {
    // catch any remaining non empty variables
    return true;
  }
  // if none of the other checks returned results we assume the variable is non-empty
  return false;
}

export { isDefined, isEmpty };
