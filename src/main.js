// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
//import WorkSpaceDataHandler from './plugins/workspace-data';
import * as _o from './assets/is';

Vue.config.productionTip = false;
//Vue.config.devtools = true;

//Vue.use(WorkSpaceDataHandler);

const localStore = function (handle) {
    return {
        getter() {
            return JSON.parse(localStorage.getItem(handle));
        },
        setter(value) {
            localStorage.setItem(handle, JSON.stringify(value));
        },
    };
};

const tabbedDataHandler = function(){
    return {
        state: [],
        async init() {
            this.state = await this.get();
        },
        async set( data ) {
            return Promise.resolve( this.setter( this.getCopy( data ) ) );
        },
        setData( data ) {
            this.state = data;
            console.log( 'saving data', data );
            this.set( data ).catch( () => {
                console.log( 'saving ffed up' );
            } );
        },
        /*
         fetches data whether from async source or not, then returns a copy of that data
         */

        async get() {
            console.log( 'get', this.getter(), this.state );
            const data = this.dataCheck( await Promise.resolve( this.getter() ) );
            return this.getCopy( data );
        },
        dataDefault() {
            const index = this.indexField;
            const contentField = this.contentField;
            const defaultObj = {};
            defaultObj[ index ] = 'new';
            defaultObj[ contentField ] = '';
            return [ defaultObj ];
        },
        getIndexByGroup( group ) {
            let found = false;
            let index = 0;
            let foundIndex = false;
            while ( !found || index > this.state.length ) {
                if ( this.state[ index ][ this.indexField ] === group ) {
                    found = true;
                    foundIndex = index;
                }
                index += 1;
            }
            return foundIndex;
        },
        getContentByIndex( index, data ) {
            if ( !_o.isDefined( data, index, this.contentField ) ) {
                return false;
            }
            return data[ index ][ this.contentField ];
        },
        dataCheck( data ) {

            // if we don't have valid data, create a default
            if ( typeof data === 'undefined' || typeof data !== 'object' ) {
                data = this.dataDefault();
            }
            // if we have an empty array, add a placeholder
            if ( data.length < 1 ) {
                data = this.dataDefault();
            } else {
                // check items are valid

                const contentField = this.contentField;
                const indexField = this.indexField;
                // remove items that have no index field set
                data = data.filter( ( item, index ) => item && item[ indexField ] );
                // make sure each item as a contentField defined
                data.forEach( ( item, index ) => {
                    if ( typeof item[ contentField ] === 'undefined' ) {
                        item[ contentField ] = '';
                    }
                } );
            }
            return data;
        },
        getSettingsCopy() {
            const data = this.get();
            return this.getCopy( data );
        },
        getCopy( data ) {
            return JSON.parse( JSON.stringify( data ) );
        },
        changeGroupName( name, oldName ) {

            const instance = this;
            let data = this.state;
            if ( !data ) {
                data = {};
            }
            const index = this.getIndexByGroup( oldName );

            if ( index > -1 ) {
                data[ index ][ instance.indexField ] = name;
            }
            this.setData( data );
        },
        addGroup( name, index, contents ) {
            if ( !contents ) {
                contents = '';
            }
            let increment = 0;
            let isUnique = true;
            let nameToUse = name;
            do {
                const indexField = this.indexField;
                const found = this.state.filter( item => item[ indexField ] === nameToUse );
                if ( found.length > 0 ) {
                    isUnique = false;
                    increment += 1;
                    nameToUse = `${name}-${increment}`;
                } else {
                    isUnique = true;
                }
            } while ( !isUnique );
            const data = this.state;
            const newSection = {
                [this.indexField]: nameToUse,
                [this.contentField]: contents,
            };
            data.splice( index, 0, newSection );
            this.setData( data );
            console.log( 'added group', this.state );
        },
        removeGroup( name ) {
            const instance = this;
            const data = this.state;
            const index = this.getIndexByGroup( name );
            if ( index !== false ) {
                data.splice( index, 1 );
                this.setData( data );
            }
        },
        reOrderGroups( moved ) {
            const data = this.state;
            console.log( 'REORDERGROUSP', moved );
            if ( moved.newIndex >= data.length ) {
                let k = moved.newIndex - data.length + 1;
                while ( k-- ) {
                    data.push( undefined );
                }
            }
            data.splice( moved.newIndex, 0, data.splice( moved.oldIndex, 1 )[ 0 ] );

            this.setData( data );
        },
        doMutation( data, index, content, group ) {
            let newData = this.getCopy( data );
            if ( _o.isDefined( newData, index ) ) {
                if ( newData[ index ][ this.indexField ] === group ) {
                    newData[ index ][ this.contentField ] = content;
                }
            } else {
                if ( !newData ) {
                    newData = [];
                }
                newData.push( { [this.indexField]: group, [this.contentField]: content } );
            }
            return newData;
        },
    }
};

const mountWorkspacePanelManager = function(){


    new Vue({
        //el: '#workspace-panels-app',
        components: { App },
        template: '<app/>',
        data() {
            return {
                panels: [],
            };
        },
        mounted(){
            console.log('workspace was mounted');
        },
        methods: {},
    }).$mount('#workspace-panels-app');//$mount().$appendTo('body')
};

/* eslint-disable no-new */
const WorkspacePanelManager = {
    state: {
        panels: {},
        openPanels: [],
    },
    handlers: {},
    addPanel(handle, opts, handler) {
        this.state.panels[handle] = opts;
        if (handler) {
            handler = localStore(handle);
        }
        if (opts.type === 'tabbed-panel') {
            handler = Object.assign(handler, tabbedDataHandler());

            handler.indexfield = opts.indexField;
            handler.contentField = opts.contentField;
        }

        this.handlers[handle] = handler;
    },
    registerPanel(handle, opts, handler) {
        const id = this.idMaker();
        const that = this;
        this.state.panels[id] = Object.assign(opts, { handle, id });
        if (!handler) {
            handler = localStore(handle);
        }
        if (opts.type === 'tabbed-panel') {
            handler = Object.assign(tabbedDataHandler(), handler);

            handler.indexField = opts.indexField;
            handler.contentField = opts.contentField;
        }
        this.handlers[handle] = handler;
        handler.init();
        console.log('setHandler', handle);
        return {
            openPanel() {
                that.open(id);
            },
            closePanel() {
                that.close(id);
            },
        };
    },
    open(id) {
        /*
         let isOpen = false;
         this.state.openPanels.foreach((panel, i){

         }); */
        this.state.openPanels.push(id);
    },
    close(id) {
        const index = this.state.openPanels.indexOf(id);
        this.state.openPanels.splice(index, 1);
    },
    isId(element) {
    },
    getPanel(id) {
        return this.state.panels[id];
    },
    getDataHandler(handle) {
        const h = this.handlers[handle];
        console.log('gettingdatahandler', h);
        return this.handlers[handle];
    },
    idMaker() {
        return (
            `_${
                Math.random()
                    .toString(36)
                    .substr(2, 9)}`
        );
    },
    copyData() {
    },
    getCopy() {
    },

    mount(){
        mountWorkspacePanelManager();
    }
};


const PanelStore = {
    install(Vue, options) {
        Vue.prototype.$openPanels = WorkspacePanelManager.state.openPanels;
    },
};

//Vue.$root.panelStore2 = WorkspacePanelManager.state.openPanels;
Vue.use(PanelStore);


window.WorkspacePanelManager = WorkspacePanelManager;

console.log('element was found?', document.querySelector('#workspace-panels-app'));
console.log('theplugin', WorkspacePanelManager);
window.WorkspacePanelManager = WorkspacePanelManager;
export default WorkspacePanelManager;


//$mount('#workspace-panels-app');
