import WorkspacePanelManager from './main.js';

const PanelStore = {
    install(Vue, options) {
        Vue.prototype.$panelStore = WorkspacePanelManager.state;
    },
};

export default PanelStore;
