
const exampleHandler = {
    state: [
        { name: 'one', css: ' from one' },
        { name: 'two', css: 'stuff from two' },
        { name: 'three', css: 'stuff from three' },
        { name: 'four', css: 'stuff from four' },
    ],
    getter() {
        console.log('exampleHandler', this);
        return this.state;
    },
    setter(value) {
        this.state = value;
    },
};

const examplePanel = {
    mode: 'less',
    type: 'tabbed-panel',
    use: 'codemirror',
    indexField: 'name',
    contentField: 'css',
    config: { mode: 'less', colorpicker : {
        mode : 'edit'
    }, },
};
const exa = WorkspacePanelManager.registerPanel(
    'css',
    examplePanel,
    exampleHandler,
);

window.exa = exa;
WorkspacePanelManager.mount();
exa.openPanel();